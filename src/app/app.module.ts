import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JobScheduleComponent } from './component/job-schedule/job-schedule.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ManageJobscheduleComponent } from './component/job-schedule/manage-jobschedule/manage-jobschedule.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgSelect2Module } from 'ng-select2';
import { HistoryDataserviceComponent } from './component/history-dataservice/history-dataservice.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
@NgModule({
  declarations: [
    AppComponent,
    JobScheduleComponent,
    ManageJobscheduleComponent,
    HistoryDataserviceComponent,
  ],
  imports: [
    AppRoutingModule,
    NgbModule,
    FormsModule,
    DataTablesModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    NgSelect2Module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
