import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobScheduleComponent } from './component/job-schedule/job-schedule.component';
import { ManageJobscheduleComponent } from './component/job-schedule/manage-jobschedule/manage-jobschedule.component';
import { HistoryDataserviceComponent } from './component/history-dataservice/history-dataservice.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'JobSchedule',
    pathMatch: 'full',
  },
  {
    path: 'JobSchedule',
    component: JobScheduleComponent,
  },
  {
    path: 'Manage-JobSchedule',
    component: ManageJobscheduleComponent,
  },
  {
    path: 'History-DataService',
    component: HistoryDataserviceComponent,
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
