import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-job-schedule',
  templateUrl: './job-schedule.component.html',
  styleUrls: ['./job-schedule.component.css']
})
export class JobScheduleComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  public data: any;
  dtTrigger: Subject<any> = new Subject();
  constructor() { }

  ngOnInit(): void {
    this.dtOptions = {
      search: true,
      pagingType: 'full_numbers',
      serverSide: true,
      processing: true,
      pageLength: 25,
      order: [1, 'desc'],
      ajax: (dataTablesParameters: any, callback) => {
      },
      columnDefs: [
        {
          targets: 0,
          orderable: false,
          data: function (data: any, type: any, row: any, meta: any) {
            return meta.row + meta.settings._iDisplayStart + 1;
          }
        }, {
          targets: 1,
          data: function (data: any, type: any, row: any, meta: any) {

          }
        }, {
          targets: 2,
          data: function (data: any, type: any, row: any, meta: any) {

          }
        }, {
          targets: 3,
          data: function (data: any, type: any, row: any, meta: any) {

          }
        }, {
          targets: 4,
          data: function (data: any, type: any, row: any, meta: any) {

          }
        }, {
          targets: 5,
          data: function (data: any, type: any, row: any, meta: any) {

          }
        }, {
          targets: 6,
          data: function (data: any, type: any, row: any, meta: any) {

          }
        }, {
          targets: 7,
          data: function (data: any, type: any, row: any, meta: any) {

          }
        }, {
          targets: 8,
          data: function (data: any, type: any, row: any, meta: any) {

          }
        }, {
          targets: 9,
          data: function (data: any, type: any, row: any, meta: any) {
            return `<button  edit-id="${data.id}" class="btn btn-info"  title="แก้ไข"><i class="fa fa-edit" edit-id="${data.id}"> แก้ไข</i></button>`;
          }
        }]
    };
  }

}
