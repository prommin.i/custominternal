import { Component, OnInit } from '@angular/core';
import { Select2OptionData } from 'ng-select2';
import { Options } from 'select2';
@Component({
  selector: 'app-manage-jobschedule',
  templateUrl: './manage-jobschedule.component.html',
  styleUrls: ['./manage-jobschedule.component.css']
})
export class ManageJobscheduleComponent implements OnInit {
  bsValue = new Date();
  public exampleData: Array<Select2OptionData>;
  public options: Options;
  files = [];

  readInput($event) {
    console.log($event.target.files);
  }
  constructor() { 
  }

  ngOnInit(): void {
    this.exampleData = [
      {
        id: '1',
        text: 'Batch File'
      },
      {
        id: '2',
        text: 'Web Service'
      },
    
    ];
    this.options = {
      theme: 'classic',
      closeOnSelect: true,
      width: '100%'
    };
  }

}
