import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageJobscheduleComponent } from './manage-jobschedule.component';

describe('ManageJobscheduleComponent', () => {
  let component: ManageJobscheduleComponent;
  let fixture: ComponentFixture<ManageJobscheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageJobscheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageJobscheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
