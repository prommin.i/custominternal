import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-history-dataservice',
  templateUrl: './history-dataservice.component.html',
  styleUrls: ['./history-dataservice.component.css']
})
export class HistoryDataserviceComponent implements OnInit {
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  
  constructor() {
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsRangeValue = [this.bsValue, this.maxDate];
   }

  ngOnInit(): void {
  }

}
