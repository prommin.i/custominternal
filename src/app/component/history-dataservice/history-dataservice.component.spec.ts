import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryDataserviceComponent } from './history-dataservice.component';

describe('HistoryDataserviceComponent', () => {
  let component: HistoryDataserviceComponent;
  let fixture: ComponentFixture<HistoryDataserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryDataserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryDataserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
